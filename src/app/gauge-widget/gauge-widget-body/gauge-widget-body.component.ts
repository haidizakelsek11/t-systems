import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-gauge-widget-body',
  templateUrl: './gauge-widget-body.component.html',
  styleUrls: ['./gauge-widget-body.component.scss']
})
export class GaugeWidgetBodyComponent implements OnInit {
  @Input() selectedView = ''; 
  diameter = 100;
  angle = 3;

  constructor() { }

  ngOnInit(): void {
  }
}
