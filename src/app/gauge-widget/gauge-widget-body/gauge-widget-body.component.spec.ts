import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GaugeWidgetBodyComponent } from './gauge-widget-body.component';

describe('GaugeWidgetBodyComponent', () => {
  let component: GaugeWidgetBodyComponent;
  let fixture: ComponentFixture<GaugeWidgetBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GaugeWidgetBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GaugeWidgetBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
