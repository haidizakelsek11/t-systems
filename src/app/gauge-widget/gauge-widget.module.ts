import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GaugeWidgetHeaderComponent } from './gauge-widget-header/gauge-widget-header.component';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { GaugeWidgetFullComponent } from './gauge-widget-full/gauge-widget-full.component'
import { DragDropModule } from '@angular/cdk/drag-drop';
import { GaugeWidgetBodyComponent } from './gauge-widget-body/gauge-widget-body.component';
import { MatButtonModule } from '@angular/material/button'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CircularSliderModule } from '../circular-slider/circular-slider.module';

@NgModule({
  declarations: [GaugeWidgetHeaderComponent, GaugeWidgetFullComponent, GaugeWidgetBodyComponent],
  imports: [
    CommonModule, MatIconModule, MatMenuModule, DragDropModule, MatButtonModule, BrowserAnimationsModule, CircularSliderModule
  ],
  exports: [ MatIconModule, GaugeWidgetFullComponent, MatMenuModule, DragDropModule, MatButtonModule,
    GaugeWidgetBodyComponent]
})

export class GaugeWidgetModule { }
