import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-gauge-widget-header',
  templateUrl: './gauge-widget-header.component.html',
  styleUrls: ['./gauge-widget-header.component.scss']
})
export class GaugeWidgetHeaderComponent {
  //@ViewChild(MatMenuTrigger) 'menu': MatMenuTrigger;
  showMenu: boolean = false;
  @Output() selectedViewEvent = new EventEmitter<string>();

  showMenuTrigger(value: string) {
    this.showMenu = !this.showMenu;
    this.selectedViewEvent.emit(value)
    console.log(value)
  }
}
