import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GaugeWidgetHeaderComponent } from './gauge-widget-header.component';

describe('GaugeWidgetHeaderComponent', () => {
  let component: GaugeWidgetHeaderComponent;
  let fixture: ComponentFixture<GaugeWidgetHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GaugeWidgetHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GaugeWidgetHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
