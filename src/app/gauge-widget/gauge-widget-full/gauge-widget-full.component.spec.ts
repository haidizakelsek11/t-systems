import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GaugeWidgetFullComponent } from './gauge-widget-full.component';

describe('GaugeWidgetFullComponent', () => {
  let component: GaugeWidgetFullComponent;
  let fixture: ComponentFixture<GaugeWidgetFullComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GaugeWidgetFullComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GaugeWidgetFullComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
