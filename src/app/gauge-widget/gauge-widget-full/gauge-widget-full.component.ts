import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gauge-widget-full',
  templateUrl: './gauge-widget-full.component.html',
  styleUrls: ['./gauge-widget-full.component.scss']
})
export class GaugeWidgetFullComponent implements OnInit {
  selectedView: string = ''

  constructor() { }

  ngOnInit(): void {
  }

  getSelectedView(value: string) {
    this.selectedView = value
  }

}
