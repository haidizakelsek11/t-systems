import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CircularSliderComponent } from './circular-slider-full/circular-slider/circular-slider.component';
import { CircularSliderHandleDirective } from './directives/circular-slider-handle.directive';
import { HalfCircularSliderComponent } from './half-circular-slider/half-circular-slider.component';
import { NgChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule, NgChartsModule
  ],
  declarations: [
    CircularSliderComponent,
    CircularSliderHandleDirective,
    HalfCircularSliderComponent,
  ],
  exports: [
    CircularSliderComponent,
    CircularSliderHandleDirective,
    HalfCircularSliderComponent
  ],
})
export class CircularSliderModule { }
