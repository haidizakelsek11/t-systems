import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HalfCircularSliderComponent } from './half-circular-slider.component';

describe('HalfCircularSliderComponent', () => {
  let component: HalfCircularSliderComponent;
  let fixture: ComponentFixture<HalfCircularSliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HalfCircularSliderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HalfCircularSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
