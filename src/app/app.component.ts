import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component } from '@angular/core';
import { GaugeWidgetFullComponent } from './gauge-widget/gauge-widget-full/gauge-widget-full.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'gauge-widget';
  test = new GaugeWidgetFullComponent;
  todo = [this.test];
  done = [];

  list: any = [GaugeWidgetFullComponent, '', '', '']

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.list, event.previousIndex, event.currentIndex);
  }
}
